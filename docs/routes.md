---
Configuration sample.
Values must be RouterOS valid parameters for routes (/ip route).

Be careful: the role only sets defined variables.
If you set a variable, apply configuration and then
you undefine the variable - no any config changes will be done.
The value will remain the same as the last defined variable.

```
# if true _ deletes All STATIC routes from routing table
mikrotik_static_routes_remove_old_routes: false

#
# dst_address, gateway and comment are REQUIRED attributes
#
mikrotik_static_routes:
  - dst_address: "10.8.1.0/24"
    gateway:
      - "10.8.0.1"
    comment: "comment 1"
    bgp_as_path:
    bgp_atomic_aggregate:
    bgp_communities:
    bgp_local_pref:
    bgp_med:
    bgp_origin:
    bgp_prepend:
    check_gateway:
    # copy_from:  _ might be done in future
    disabled:
    distance:
    pref_src:
    route_tag:
    routing_mark:
    scope:
    target_scope:
    type:

  - dst_address: "10.8.2.0/24"
    gateway:
      - "10.8.0.1"
    comment: "comment 1"
    bgp_as_path:
    bgp_atomic_aggregate:
    bgp_communities:
    bgp_local_pref:
    bgp_med:
    bgp_origin:
    bgp_prepend:
    check_gateway:
    # copy_from:  _ might be done in future
    disabled:
    distance:
    pref_src:
    route_tag:
    routing_mark:
    scope:
    target_scope:
    type:
```
