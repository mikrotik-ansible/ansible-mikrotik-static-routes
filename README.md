# Mikrotik RouterOS users and SSH public keys management
Allows adding and deleting static routes for Mikrotik RouterOS

Big thanks to Martin Dulin for his role https://github.com/mikrotik-ansible/mikrotik-firewall.
His role gave me an idea how solve RouterOS configuration tasks.
